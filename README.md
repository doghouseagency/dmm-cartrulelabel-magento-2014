# Cart Rule Label

Easily show cart rule labels (i.e. "Free Shipping for orders over $100!") anywhere on your site.

There is a config in which you can specify the cart rule id and whether the label is enabled or disabled

You can also decide to only show the label if the rule hasn't been applied yet.

## API

### Usage

````
<?php if($_promotionLabel = Mage::helper('cartrulelabel')->getLabel(true)) : ?>
    <p><?php echo $_promotionLabel ?></p>
<?php endif; ?>

````
### Methods

---

`isEnabled`

Returns system config

---

`getRuleId`

Returns system config

---

`canShow`

Returns true if rule was recognized and label is enabled

---

`getRule`

Returns `Mage_SalesRule_Model_Rule`

---

`isApplied`

Returns whether the rule specified in system config is applied or not(bool)

---

`getLabel($showOnlyIfNotApplied = true)`

Returns the label of the rule, or an empty string depending on the parameter

---

