<?php

class Doghouse_CartRuleLabel_Helper_Data extends Mage_Core_Helper_Abstract {

    CONST XML_PATH_ENABLED = 'promo/cartrulelabel/enabled';
    CONST XML_PATH_RULE_ID = 'promo/cartrulelabel/rule_id';

    private $_rule = null;

    public function isEnabled() {
        return Mage::getStoreConfig(self::XML_PATH_ENABLED);
    }

    public function getRuleId() {
        return Mage::getStoreConfig(self::XML_PATH_RULE_ID);
    }

    public function canShow() {
        return $this->isEnabled() && $this->getRule();
    }

    public function getRule() {
        if(!$this->_rule) {
            $rule = Mage::getResourceModel('salesrule/rule_collection')
                ->addFieldToFilter('rule_id', $this->getRuleId())
                ->addWebsiteGroupDateFilter(
                    Mage::app()->getWebsite()->getId(),
                    Mage::getSingleton('customer/session')->getCustomerGroupId()
                )->getFirstItem();
            if($rule->getId()) {
                $this->_rule = $rule;
            }
        }
        return $this->_rule;
    }

    public function isApplied() {
        $quote = Mage::getModel('checkout/cart')->getQuote();
        $ruleIds = explode(",", $quote->getAppliedRuleIds());

        if(in_array($this->getRule()->getId(), $ruleIds)) {
            return true;
        }

        return false;
    }

    public function getLabel($showOnlyIfNotApplied = true) {

        if($this->canShow()) {

            if(!$showOnlyIfNotApplied) {
                return $this->getRule()->getStoreLabel();
            }

            if(!$this->isApplied()) {
                return $this->getRule()->getStoreLabel();
            }

        }

        return '';
    }

}
